# Check if we're on TTY1 and no graphical session is active
if [[ -z $DISPLAY && $(tty) != /dev/tty[2-9] ]]; then
      exec startx
fi
